<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ListController extends Controller
{
    public function all() {
        $users = User::all();
        return response()->json($users);
    }
}
